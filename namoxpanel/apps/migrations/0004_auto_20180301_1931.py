# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-03-02 01:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0003_auto_20180301_1928'),
    ]

    operations = [
        migrations.AlterField(
            model_name='app',
            name='db_engine',
            field=models.CharField(blank=True, choices=[('-', ''), ('postgres', 'PostgreSQL'), ('mysql', 'MySql'), ('mariadb', 'MariaDB'), ('sqlite', 'SQLite')], default='-', max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='app',
            name='output',
            field=models.CharField(blank=True, choices=[('-', ''), ('json', 'JSON'), ('yalm', 'YALM')], default='-', max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='app',
            name='product',
            field=models.CharField(blank=True, choices=[('-', ''), ('rest', 'REST'), ('soap', 'SOAP'), ('websocket', 'WebSocket'), ('e-commerce', 'E-Commerce'), ('e-commerce-api', 'E-commerce API'), ('blog', 'Blog'), ('blog-api', 'Blog API'), ('personal-page', 'Personal Page')], default='-', max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='app',
            name='technology',
            field=models.CharField(blank=True, choices=[('-', ''), ('flask', 'Flask - Python'), ('django', 'Django - Python'), ('drf', 'Django Rest Framework - Python'), ('django_drf', 'Django With Django Rest Framework - Python'), ('apistar', 'Apistar - Python'), ('plain_php', 'Plan PHP - PHP'), ('plain_php_7', 'Plan PHP 7 - PHP'), ('mamp_php', 'MAMP - PHP'), ('lamp_php', 'LAMP - PHP'), ('slim_php', 'Slim PHP - PHP'), ('ci_php', 'CodeIgniter - PHP'), ('symfony', 'Symfony - PHP'), ('yii', 'Yii - PHP'), ('yii_2', 'Yii 2 - PHP'), ('wp', 'WordPress - PHP'), ('buffalo-go', 'Buffalo - Go'), ('mean-js', 'MEAN - JavaScript'), ('apollo-server-js', 'Apollo Server - JavaScript'), ('parse-js', 'Parse - JavaScript')], default='-', max_length=200, null=True),
        ),
    ]
